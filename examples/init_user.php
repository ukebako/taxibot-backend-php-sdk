<?php
require_once __DIR__.'/../vendor/autoload.php';

use TaxiBot\Backend\Api\Api;
use TaxiBot\Backend\Api\ServerException;
use TaxiBot\Backend\Api\ClientException;

$api = new Api('secret', 'http://backend.taxibot.local:8000');

$chat_user_id = '9876543';

try {
    $result = $api->exec('/client/get_user', [
        'chat_user_id' => $chat_user_id,
    ]);
}
catch (ServerException $e) {
    if ($e->getCode() == 401 && $e->origin == 'client') { // chat user is not authorized
        $result = $api->exec('/client/init_user', [
            'chat_user_id' => $chat_user_id,
            'chat_user_name' => 'Фредобок Хотелкин',
            'messenger' => 'viber',
            'phone' => '380501112233',
            'avatar' => 'http://i0.kym-cdn.com/photos/images/original/000/614/948/36c.jpg',
        ]);
    }
}
catch (ClientException $e) {
    echo "Something went wrong...\n";
    throw ($e);
}

print_r($result);
