<?php namespace TaxiBot\Backend\Api;

/**
 * Client exception for errors like connection exception etc...
 */
class ClientException extends \Exception
{
}
